import os
import sys
from pytube import YouTube
from pytube import Playlist
from rich.console import Console
from rich.progress import track

console = Console()

# Download Only Video :
def download_video() :
    path = input('Enter The path for Save 📁 >> : ')
    os.chdir(path)
    
    url = input('Enter your video url 🔗 >> : ')
    video = YouTube(url)

    #print('Video title >> : '+ video.title)
    #print('Video thumbnail >> : '+ video.thumbnail_url)

    # Choosing Resolution : 
    console.print('''enter video resulution >> : 
    [1] ▶️  144p mp4
    [2] ▶️  360p mp4
    [3] ▶️  480p mp4
    [4] ▶️  720p mp4
    [5] ▶️  1080p mp4
    [6] ▶️  1080p 60fps mp4
    ''', style="bold magenta")

    # Choosing Resolution :
    choice = input('Enter resolution number >> : ')

    if choice == '1': #144p
        video.streams.filter(mime_type="video/mp4", res="144p", subtype="mp4").first().download(filename=video.title + '(144p).mp4')

    elif choice == '2': #360p
        video.streams.filter(mime_type="video/mp4", res='360p', subtype="mp4").first().download(filename=video.title + '(360p).mp4')

    elif choice == '3': #480p
        video.streams.filter(mime_type="video/mp4", res='480p', subtype="mp4").first().download(filename=video.title + '(480p).mp4')

    elif choice == '4': #720p
        video.streams.filter(mime_type="video/mp4", res='720p', subtype="mp4").first().download(filename=video.title + '(720p).mp4')

    elif choice == '5': #1080p
        video.streams.filter(mime_type="video/mp4", res='1080p', subtype="mp4").first().download(filename=video.title + '(1080p).mp4')

    elif choice == '6': #1080p,60fps
        video.streams.filter(mime_type="video/mp4", res='1080p', fps="60fps", subtype="mp4").first().download(filename=video.title + '(1080p,60fps).mp4')
    
    else :
        print('Input Error ❌', style="bold red")

    console.print(f'Downloading ⬇️  : {video.title}', style="bold cyan")
    for step in track(range(100)):
        (step)
    console.print('Download is complete ✅', style="bold green")


# Download Audio :
def download_audio():
    path = input('Enter The path for Save 📁 >> : ')
    os.chdir(path)
    
    url = input('Enter your video url 🔗 >> : ')
    video = YouTube(url)

    # Extract Audio :
    video.streams.filter(only_audio=True).first().download(filename=video.title + '.mp3')
    
    console.print(f'Downloading ⬇️  : {video.title}', style="bold cyan")
    for step in track(range(100)):
        (step)
    console.print('Download is complete ✅', style="bold green")
# Download Playlist :
def download_playlist():
    path = input('Enter The path for Save 📁 >> : ')
    os.chdir(path)

    playlist = Playlist(input('Enter the Playlist url 🔗 >> : '))

    # Get video Number in playlist :
    print('Number of videos in playlist: %s' % len(playlist.video_urls))
    for video_url in playlist.video_urls:
        print(video_url)
    
    # Choosing Resolution : 
    console.print('''enter video resulution >> : 
    [1] ▶️  144p mp4
    [2] ▶️  360p mp4
    [3] ▶️  480p mp4
    [4] ▶️  720p mp4
    [5] ▶️  1080p mp4
    [6] ▶️  1080p 60fps mp4
    ''', style="bold magenta")
    choice = input('Enter resolution number >> : ')

    if choice == '1': # 144p
        print(f'Downloading: {playlist.title}')
        for video in playlist.videos:
            video.streams.filter(mime_type="video/mp4", res='144p').first().download(f'{playlist.title}', filename=video.title + '(144p).mp4')
        
    elif choice == '2': # 360p
        print(f'Downloading: {playlist.title}')
        for video in playlist.videos:
            video.streams.filter(mime_type="video/mp4", res='360p').first().download(filename=video.title + '(360p).mp4')
    
    elif choice == '3': # 480p
        print(f'Downloading: {playlist.title}')
        for video in playlist.videos:
            video.streams.filter(mime_type="video/mp4", res='480p').first().download(filename=video.title + '(480p).mp4')
    
    elif choice == '4': # 720p
        print(f'Downloading: {playlist.title}')
        for video in playlist.videos:
            video.streams.filter(mime_type="video/mp4", res='720p').first().download(filename=video.title + '(720p).mp4')
    
    elif choice == '5': # 1080p
        print(f'Downloading: {playlist.title}')
        for video in playlist.videos:
            video.streams.filter(mime_type="video/mp4", res='1080p').first().download(filename=video.title + '(1080p).mp4')

    elif choice == '6': # 1080p 60fps
        print(f'Downloading: {playlist.title}')
        for video in playlist.videos:
            video.streams.filter(mime_type="video/mp4", res='1080p', fps="60fps").first().download(filename=video.title + '(1080p,60fps).mp4')
    else :
        print('Input Error !')
    console.print(f'Downloading ⬇️  : {video.title}', style="bold cyan")
    for step in track(range(100)):
        (step)
    console.print('Download is complete ✅', style="bold green")

# Download Type :
console.print('OpenTube ', style='bold blue')
console.print('Open Source YouTube Video 🎞️ Downloader ⬇️  😇', style="bold magenta")
console.print(''' 
[1] > Video 🎞️
[2] > Audio 🎵
[3] > Playlist ⏯️ ''', style="bold cyan")
download_type = input('[*] choose download ⬇️  type >> : ')

if download_type == '1':
    download_video()
elif download_type == '2':
    download_audio()
elif download_type == '3':
    download_playlist()
else :
    console.print('Input Error ❌', style="bold red" "")
